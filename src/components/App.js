import React from 'react';
import Header from './Header'

const items = [
    {
        id: 0,
        name: 'Air Filter',
        part_number: '123456',
        price: 8.99,
        brand: 'Briggs and Stratton'
    },
    {
        id: 1,
        name: 'Needle Kit',
        part_number: '398188',
        price: 8.45,
        brand: 'Briggs and Stratton'
    },
    {
        id: 2,
        name: 'Spark Plug',
        part_number: 'BPM8Y',
        price: 4.99,
        brand: 'NGK'
    },
    {
        id: 3,
        name: 'Spark Plug',
        part_number: 'RC12YC',
        price: 4.99,
        brand: 'Champion'
    },
    {
        id: 4,
        name: 'Fuel Line',
        part_number: '01234',
        price: 3.99,
        brand: 'Briggs and Stratton'
    },
]

class App extends React.Component {
    state = {
        cart: []
    }
    
    renderItems = () => {
        return items.map(item => {
            return (
                <div className='col-sm' key={item.id}>
                    <p>Item name: {item.name}</p>
                    <p>Part Number: {item.part_number}</p>
                    <p>Brand: {item.brand}</p>
                    <p>Price: ${item.price}</p>
                    <button onClick={() => this.addToCart(item.id, item.name, item.part_number, item.brand, item.price)}>Add to Cart</button>
                </div>
            )
        })
    }

    addToCart = (id, name, part_number, brand, price) => {
        // console.log(id, name, part_number, brand, price)

        let item = {
            id,
            name,
            part_number,
            brand,
            price
        }

        let newArray = this.state.cart.slice()

        newArray.push(item)
        
        this.setState({
            cart: newArray
        })
    }

    renderCart = () => {
        return this.state.cart.map(item => {
            return (
                <div>
                    <div className='col-sm'>
                        <p className='row'>Item name: {item.name}</p>
                        <p className='row'>Part number: {item.part_number}</p>
                        <p className='row'>Brand: {item.brand}</p>
                        <p className='row'>Price: ${item.price}</p>
                        <button onClick={() => this.removeItem(item)}>Remove Item</button>
                    </div>
                </div>
            )
        })
    }

    removeItem = item => {
        let newArr = [...this.state.cart]

        let index = newArr.indexOf(item)

        if (index !== -1) {
            newArr.splice(index, 1)
            this.setState({
                cart: newArr
            })
        }
    }

    render() {
        return (
            <div className='container'>
                <Header />
                <div className='row'>
                    {this.renderItems()}
                </div>
                <div className='row'>
                    <div className='col-sm'>
                        Cart
                        {this.renderCart()}
                        {this.state.cart.length > 0 ? (<button>Charge</button>) : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default App;